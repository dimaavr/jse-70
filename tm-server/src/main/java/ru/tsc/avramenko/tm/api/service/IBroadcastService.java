package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.avramenko.tm.enumerated.EntityOperationType;

import javax.jms.JMSException;

public interface IBroadcastService {

    void sendJmsMessageAsync(
            @NotNull Object entity,
            @NotNull EntityOperationType operationType
    );

    void sendJmsMessageSync(
            @NotNull Object entity, @NotNull EntityOperationType operationType
    ) throws JMSException;

    void shutdown();

    void submit(@NotNull Runnable runnable);

}