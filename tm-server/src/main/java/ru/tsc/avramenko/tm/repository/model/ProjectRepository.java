package ru.tsc.avramenko.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsc.avramenko.tm.model.Project;
import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, String> {

    @Query("SELECT e FROM Project e WHERE e.user.id = :userId and e.name = :name")
    @Nullable Project findByName(
            @Param("userId") @NotNull String userId,
            @Param("name") @NotNull String name
    );

    @Query("SELECT e FROM Project e WHERE e.user.id = :userId")
    @Nullable List<Project> findByIndex(
            @Param("userId") @NotNull String userId
    );

    @Modifying
    @Query("DELETE FROM Project e WHERE e.user.id = :userId and e.name = :name")
    void removeByName(
            @Param("userId") @NotNull String userId,
            @Param("name") @NotNull String name
    );

    @Modifying
    @Query("DELETE FROM Project e")
    void clear();

    @Modifying
    @Query("DELETE FROM Project e WHERE e.user.id = :userId")
    void clear(@Param("userId") @NotNull String userId);

    @Query("SELECT e FROM Project e WHERE e.user.id = :userId and e.id = :id")
    @Nullable Project findById(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    @Modifying
    @Query("DELETE FROM Project e WHERE e.user.id = :userId and e.id = :id")
    void removeById(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    @Query("SELECT e FROM Project e WHERE e.user.id = :userId")
    @Nullable List<Project> findAllById(
            @Param("userId") @NotNull String userId
    );

}