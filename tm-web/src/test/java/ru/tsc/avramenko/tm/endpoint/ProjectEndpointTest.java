package ru.tsc.avramenko.tm.endpoint;

import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.avramenko.tm.client.ProjectEndpointClient;
import ru.tsc.avramenko.tm.marker.WebCategory;
import ru.tsc.avramenko.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

@Ignore
public class ProjectEndpointTest {

    final Project project = new Project("Project1");

    final Project project2 = new Project("Project2");

    final ProjectEndpointClient endpoint = ProjectEndpointClient.client();

    @BeforeClass
    @Category(WebCategory.class)
    public static void beforeClass() {
        ProjectEndpointClient.client().deleteAll();
    }

    @Before
    @Category(WebCategory.class)
    public void before() {
        endpoint.create(project);
    }

    @After
    @Category(WebCategory.class)
    public void after() {
        endpoint.deleteAll();
    }

    @Test
    @Category(WebCategory.class)
    public void find() {
        Assert.assertEquals(project.getName(), endpoint.find(project.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void create() {
        Assert.assertNotNull(endpoint.find(project.getId()));
    }

    @Test
    @Category(WebCategory.class)
    public void update() {
        final Project updatedProject = endpoint.find(project.getId());
        updatedProject.setName("Project1_Upd");
        endpoint.save(updatedProject);
        Assert.assertEquals("Project1_Upd", endpoint.find(project.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void delete() {
        endpoint.delete(project.getId());
        Assert.assertNull(endpoint.find(project.getId()));
    }

    @Test
    @Category(WebCategory.class)
    public void findAll() {
        Assert.assertEquals(1, endpoint.findAll().size());
        endpoint.create(project2);
        Assert.assertEquals(2, endpoint.findAll().size());
    }

    @Test
    @Category(WebCategory.class)
    public void updateAll() {
        endpoint.create(project2);
        final Project updatedProject = endpoint.find(project.getId());
        updatedProject.setName("Project1_Upd");
        final Project updatedProject2 = endpoint.find(project2.getId());
        updatedProject2.setName("Project2_Upd");
        List<Project> updatedList = new ArrayList<>();
        updatedList.add(updatedProject);
        updatedList.add(updatedProject2);
        endpoint.saveAll(updatedList);
        Assert.assertEquals("Project1_Upd", endpoint.find(project.getId()).getName());
        Assert.assertEquals("Project2_Upd", endpoint.find(project2.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void createAll() {
        endpoint.delete(project.getId());
        List<Project> list = new ArrayList<>();
        list.add(project);
        list.add(project2);
        endpoint.createAll(list);
        Assert.assertEquals("Project1", endpoint.find(project.getId()).getName());
        Assert.assertEquals("Project2", endpoint.find(project2.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void deleteAll() {
        endpoint.create(project2);
        Assert.assertEquals(2, endpoint.findAll().size());
        endpoint.deleteAll();
        Assert.assertEquals(0, endpoint.findAll().size());
    }

}