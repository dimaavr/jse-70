package ru.tsc.avramenko.tm.endpoint;

import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.avramenko.tm.client.TaskEndpointClient;
import ru.tsc.avramenko.tm.marker.WebCategory;
import ru.tsc.avramenko.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

@Ignore
public class TaskEndpointTest {

    final Task task = new Task("Task1");

    final Task task2 = new Task("Task2");

    final TaskEndpointClient endpoint = TaskEndpointClient.client();

    @BeforeClass
    @Category(WebCategory.class)
    public static void beforeClass() {
        TaskEndpointClient.client().deleteAll();
    }

    @Before
    @Category(WebCategory.class)
    public void before() {
        endpoint.create(task);
    }

    @After
    @Category(WebCategory.class)
    public void after() {
        endpoint.deleteAll();
    }

    @Test
    @Category(WebCategory.class)
    public void find() {
        Assert.assertEquals(task.getName(), endpoint.find(task.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void create() {
        Assert.assertNotNull(endpoint.find(task.getId()));
    }

    @Test
    @Category(WebCategory.class)
    public void update() {
        final Task updatedTask = endpoint.find(task.getId());
        updatedTask.setName("Task1_Upd");
        endpoint.save(updatedTask);
        Assert.assertEquals("Task1_Upd", endpoint.find(task.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void delete() {
        endpoint.delete(task.getId());
        Assert.assertNull(endpoint.find(task.getId()));
    }

    @Test
    @Category(WebCategory.class)
    public void findAll() {
        Assert.assertEquals(1, endpoint.findAll().size());
        endpoint.create(task2);
        Assert.assertEquals(2, endpoint.findAll().size());
    }

    @Test
    @Category(WebCategory.class)
    public void updateAll() {
        endpoint.create(task2);
        final Task updatedTask = endpoint.find(task.getId());
        updatedTask.setName("Task1_Upd");
        final Task updatedTask2 = endpoint.find(task2.getId());
        updatedTask2.setName("Task2_Upd");
        List<Task> updatedList = new ArrayList<>();
        updatedList.add(updatedTask);
        updatedList.add(updatedTask2);
        endpoint.saveAll(updatedList);
        Assert.assertEquals("Task1_Upd", endpoint.find(task.getId()).getName());
        Assert.assertEquals("Task2_Upd", endpoint.find(task2.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void createAll() {
        endpoint.delete(task.getId());
        List<Task> list = new ArrayList<>();
        list.add(task);
        list.add(task2);
        endpoint.createAll(list);
        Assert.assertEquals("Task1", endpoint.find(task.getId()).getName());
        Assert.assertEquals("Task2", endpoint.find(task2.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void deleteAll() {
        endpoint.create(task2);
        Assert.assertEquals(2, endpoint.findAll().size());
        endpoint.deleteAll();
        Assert.assertEquals(0, endpoint.findAll().size());
    }

}