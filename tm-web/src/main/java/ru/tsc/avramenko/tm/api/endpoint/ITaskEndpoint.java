package ru.tsc.avramenko.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.tsc.avramenko.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskEndpoint {

    @WebMethod
    @GetMapping("/find/{id}")
    Task find(@PathVariable("id") final String id);

    @WebMethod
    @GetMapping("/findAll")
    Collection<Task> findAll();

    @WebMethod
    @PostMapping("/create")
    Task create(@RequestBody final Task task);

    @WebMethod
    @PostMapping("/createAll")
    List<Task> createAll(@RequestBody final List<Task> tasks);

    @WebMethod
    @PostMapping("/save")
    Task save(@RequestBody final Task task);

    @WebMethod
    @PostMapping("/saveAll")
    List<Task> saveAll(@RequestBody final List<Task> tasks);

    @WebMethod
    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") final String id);

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll();

}