package ru.tsc.avramenko.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.avramenko.tm.api.IPropertyService;

import java.util.*;

@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    @Value("${entityForLog}")
    private String entities;

    @NotNull
    @Value("${filePath.project}")
    private String filePathProject;

    @NotNull
    @Value("${filePath.session}")
    private String filePathSession;

    @NotNull
    @Value("${filePath.task}")
    private String filePathTask;

    @NotNull
    @Value("${filePath.user}")
    private String filePathUser;

    @Nullable
    @Override
    public String getFilePath(@Nullable final String className) {
        if (!Optional.ofNullable(className).isPresent()) return null;
        switch (className) {
            case "SessionDTO":
            case "Session":
                return filePathSession;
            case "TaskDTO":
            case "Task":
                return filePathTask;
            case "UserDTO":
            case "User":
                return filePathUser;
            case "ProjectDTO":
            case "Project":
                return filePathProject;
            default:
                return null;
        }
    }

    @Override
    @NotNull
    public List<String> getLogEntities() {
        if (!Optional.ofNullable(entities).isPresent())
            return new ArrayList<>();
        return Arrays.asList(entities.split(","));
    }

}